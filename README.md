# README #

The purpose of this project is to demonstrate usage of XPath Namespaces and applying multiple assertions using Loop Controller against the same response


### Running the test ###

* Put all the files from /bin folder of this project under /bin folder of your JMeter installation
* Add **xpath.namespace.config=xpath.properties** line to *user.properties* file (user.properties file lives under /bin folder of your JMeter installation)
* Start JMeter from /bin folder
* Open **XPathAssertionDemo.jmx** file in JMeter GUI
* See View Results Tree listener for output

### Reference Material ###

* [Example Web Service](http://www.w3schools.com/webservices/tempconvert.asmx) from W3Schools is being used for the demo
* [How to use BeanShell: JMeter's favorite built-in component](http://blazemeter.com/blog/queen-jmeters-built-componentshow-use-beanshell) guide explains Beanshell Sampler's use cases and provides a form of cookbook on frequently required actions
* [XPath Language Specification](http://www.w3.org/TR/xpath/) - general information on XPath language including namespaces feature
* [XPath Assertion](http://jmeter.apache.org/usermanual/component_reference.html#XPath_Assertion) - JMeter User Manual Entry for the XPath Assertion


